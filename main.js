// constructor function for the books
function Book(title, author, maxPages, onPage) {
    this.title = title;
    this.author = author;
    this.maxPages = maxPages;
    this.onPage = onPage;
    this.print = function(){
       if (this.maxPages === this.onPage) {
             return `<li>You already read ${this.title} by ${this.author}</li>`;
        } else {
             return `<li>You still need to read ${this.title} by ${this.author}</li>`;
          }
    };
    this.printTr = function(){
        return `<tr><td>Title</td><td>${this.title}</td><td>Author</td><td>${this.author}</td><td>maxPages</td><td>${this.maxPages}</td><td>onPage</td><td>${this.onPage}</td><td>Progress</td><td><div class="myProgress"><div class="myBar">${this.percent()}%</div> </div></td></tr>`;
    };
    this.percent = function() {
         var percent = Math.round(this.onPage/this.maxPages*100);
         return percent;
        
    }

}

// books      

var lotr = new Book("Lord of the rings", "J.R.R. Tolkien", 560, 560);
var hp = new Book("Harry Potter", "J.K. Rowling", 450, 300);
var inferno = new Book("Inferno", "Dan Brown", 300, 220);
var books = [lotr, hp, inferno];

// Printing the list

for ( var i = 0; i < books.length; i++) {
document.getElementById("list").innerHTML += books[i].print();
}

// Printing the table

function printTable(){
    var tableBody = '';
    for(var i=0; i<books.length; i++) {
        tableBody += books[i].printTr();
    }

    document.getElementById("table").innerHTML = "<table>"+tableBody+"</table>";

    var loop = document.getElementsByClassName("myBar");
    for(var j=0; j<loop.length; j++){

        var bar = document.getElementsByClassName("myBar")[j];
               bar.style.width = books[j].percent() + "%";
    }
}
printTable();

// adding a new book

document.getElementById("button").addEventListener("click", addBook);

function addBook(){
var name = prompt("Please enter the name of your book");
var author = prompt("Please enter the author of the book");
var pages = parseInt(prompt(" Number of book pages"));
var whatPage = parseInt(prompt("At what page you are at?"));

var newBook = new Book(name, author, pages, whatPage);
books.push(newBook);
printTable();

}